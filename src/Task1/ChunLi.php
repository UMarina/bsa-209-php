<?php

declare(strict_types=1);

namespace App\Task1;

class ChunLi extends Fighter
{
    protected $id = 2;
    protected $name = 'Chun-Li';
    protected $image = 'https://bit.ly/2Vie3lf';

    public function __construct(int $health, int $attack)
    {
        $this->health = $health;
        $this->attack = $attack;

    }
}