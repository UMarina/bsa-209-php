<?php

declare(strict_types=1);

namespace App\Task1;

class Ryu extends Fighter
{
    protected $id = 1;
    protected $name = 'Ryu';
    protected $image = 'https://bit.ly/2E5Pouh';

    public function __construct(int $health, int $attack)
    {
        $this->health = $health;
        $this->attack = $attack;

    }
}