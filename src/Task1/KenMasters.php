<?php

declare(strict_types=1);

namespace App\Task1;

class KenMasters extends Fighter
{
    protected $id = 3;
    protected $name = 'Ken Masters';
    protected $image = 'https://bit.ly/2VZ2tQd';

    public function __construct(int $health, int $attack)
    {
        $this->health = $health;
        $this->attack = $attack;

    }
}