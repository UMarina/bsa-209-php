<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters = [];

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $max_power = 0;
        foreach ($this->fighters as $fighter){
            if($fighter->getAttack() > $max_power){
                $max_power = $fighter->getAttack();
                $most_powerful = $fighter;
            }
        }
       return $most_powerful;
    }

    public function mostHealthy(): Fighter
    {
        $max_health = 0;
        foreach ($this->fighters as $fighter){
            if($fighter->getHealth() > $max_health){
                $max_health = $fighter->getHealth();
                $most_healthy = $fighter;
            }
        }
        return $most_healthy;
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
