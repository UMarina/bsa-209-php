<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\FightArena;
use App\Task3\FightArenaHtmlPresenter;
use App\Task1\Fighter;
use App\Task1\{ChunLi,Ryu,KenMasters};

$arena = new FightArena();
$arena->add(new Ryu(100,10));
$arena->add(new ChunLi(70,30));
$arena->add(new KenMasters(80,20));
$presenter = new FightArenaHtmlPresenter();
$presentation = $presenter->present($arena);



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
    <style>
        body {
            background-color: #666666;
            font-size: 35px;
            text-shadow: 1px 1px #0c5460,
            2px 2px #0F192A;
            color:#fbfcfc;
            text-align: center;
        }
        .arena {
            margin-top: 15vh;
            position: center;
            display: flex;
            justify-content: space-around;
            width: 100%;
            align-items: center;
        }
        h2 {
            text-transform: uppercase;
            font-size: 45px;
        }
        .fighter img{
            max-height: 350px;
            display: block;
            text-align: center;
        }
        .fighter:hover {
            transform: scale(1.2);
        }
        @media only screen and (max-width : 1199px) {
            .arena {
               flex-direction: column;
            }
            .fighter {
                margin-bottom: 15vh;
            }
        }
    </style>
</head>
<body>
    <?php echo $presentation; ?>
</body>
</html>
