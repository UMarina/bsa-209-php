<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $html = '<h2>Fighters</h2><div class="arena">';

        if (count($arena->all()) > 0){

            foreach ($arena->all() as $fighter) {
                $html .= '<div class="fighter"><img src="' . $fighter->getImage() . '"><span>'.$fighter->getName().': '.$fighter->getHealth().', '.$fighter->getAttack().'</div>';
            }

        } else {

            $html .= '<p>Arena empty! Add fighters.</p>';

        }

        $html .= '</div>';

        return $html;
    }
}
